package ru.argds.app.elastic.rest.server;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"ru.argds.app.elastic.rest.common"})
@Configuration
public class AppConfiguration {
}
